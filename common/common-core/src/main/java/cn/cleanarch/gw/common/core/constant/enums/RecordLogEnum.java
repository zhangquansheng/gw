package cn.cleanarch.gw.common.core.constant.enums;

public enum RecordLogEnum {
    LocalFile,
    Elasticsearch,
    Mongodb,
    Mysql,
    Oracle,
    Clickhouse,
}
